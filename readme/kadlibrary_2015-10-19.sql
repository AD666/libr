# ************************************************************
# Sequel Pro SQL dump
# Version 4563
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.27-debug)
# Database: kadlibrary
# Generation Time: 2015-10-19 19:08:39 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table authors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `authors`;

CREATE TABLE `authors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;

INSERT INTO `authors` (`id`, `first_name`, `last_name`)
VALUES
	(1,'Lev','Tolstoy'),
	(4,'Dina','Svorovsky'),
	(6,'Lena','Okla'),
	(9,'Zheka','Peka');

/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table publication_author
# ------------------------------------------------------------

DROP TABLE IF EXISTS `publication_author`;

CREATE TABLE `publication_author` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `publication_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `publication_author` WRITE;
/*!40000 ALTER TABLE `publication_author` DISABLE KEYS */;

INSERT INTO `publication_author` (`id`, `publication_id`, `author_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,2,2),
	(4,3,1),
	(5,3,3),
	(6,4,3),
	(7,3,2),
	(16,8,2),
	(17,8,4),
	(18,8,1),
	(19,8,2),
	(20,8,3),
	(21,9,2),
	(22,10,1),
	(23,10,2),
	(24,10,3),
	(25,11,3),
	(26,11,4),
	(27,9,2),
	(28,9,3),
	(29,9,10),
	(30,9,11),
	(31,9,5),
	(32,9,1);

/*!40000 ALTER TABLE `publication_author` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table publications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `publications`;

CREATE TABLE `publications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `publishing_city` varchar(255) DEFAULT NULL,
  `publishing_year` varchar(255) DEFAULT NULL,
  `publishing_office` varchar(255) DEFAULT NULL,
  `pages` varchar(255) DEFAULT NULL,
  `graphics` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `contains` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `publications` WRITE;
/*!40000 ALTER TABLE `publications` DISABLE KEYS */;

INSERT INTO `publications` (`id`, `title`, `publishing_city`, `publishing_year`, `publishing_office`, `pages`, `graphics`, `type`, `contains`)
VALUES
	(1,'Diablo','Odessa','2015','izual','666','graphics','science','evil'),
	(2,'Starcraft','Miami','1999','Insignia','432','graphics','science','good'),
	(3,'Warcraft','San Francisco','2004','Thrall','111','none','story','evil'),
	(4,'Hearthstone','Tokyo','2009','Han','723','none','science','good'),
	(8,'olek','','','','','','',NULL),
	(9,'kitse','odessae','2010','none','23','none','story','11123');

/*!40000 ALTER TABLE `publications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `is_admin`, `remember_token`)
VALUES
	(1,'admin','$1$hJx6Zas7$BOp3c3.TeiEhL3ny3toxB1',1,'R3cbQVLhmYHW73hmpIoI0Xm941vgeMeaPpRTfUfvCEWEF4SD4tvnNUmzWVVB');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/search') }}">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label class="col-md-1 control-label">Publishing title</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="title">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Author</label>
                    <div class="col-md-3">
                        <label class="control-label">First Name</label>
                        <input type="text" class="form-control" name="author[first_name]">
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Last Name</label>
                        <input type="text" class="form-control" name="author[last_name]">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">City</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="publishing_city">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Year</label>
                    <div class="col-md-3">
                        <input type="number" class="form-control" name="publishing_year">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Office</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="publishing_office">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Number of pages</label>
                    <div class="col-md-3">
                        <input type="number" class="form-control" name="pages">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Graphics</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="graphics">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Type</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="type">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Contains</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="contains">
                    </div>
                </div>

                {{--<div class="form-group">--}}
                    {{--<label class="col-md-1 control-label">Publications</label>--}}
                    {{--<div class="col-md-3">--}}
                        {{--<select name="publications[]" class="form-control" multiple>--}}
                            {{--@foreach ($publications as $publication)--}}
                                {{--<option value={!! $publication->id !!}>{!! $publication->title !!}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="form-group">
                    <div class="col-md-1 col-md-offset-1">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>

                <hr>

                <div class="results">
                    @if (count($results) > 0)
                        <div class="row">
                            <table class="table table-striped">
                                <tr>
                                    <th>Title</th>
                                    <th>Authors</th>
                                    <th>Publishing city</th>
                                    <th>Publishing year</th>
                                    <th>Publishing office</th>
                                    <th>Number of pages</th>
                                    <th>Publications type</th>
                                </tr>
                                @foreach($results as $key => $value)
                                    <tr>
                                        <td>{!! $value->title !!}</td>
                                        <td>
                                            @foreach($value->authors as $author)
                                                {!! $author->first_name !!}
                                                {!! $author->last_name !!};
                                            @endforeach
                                        </td>
                                        <td>{!! $value->publishing_city !!}</td>
                                        <td>{!! $value->publishing_year !!}</td>
                                        <td>{!! $value->publishing_office !!}</td>
                                        <td>{!! $value->pages !!}</td>
                                        <td>{!! $value->type !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @else
                        <h3 class="text-center">
                            No Results
                        </h3>
                    @endif
                </div>
            </form>

        </div>
    </div>
@stop
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>Kad Library</title>

    {!! Html::style( asset('bootstrap/css/bootstrap.min.css')) !!}
    {!! Html::style( asset('css/styles.css')) !!}

</head>
<body>

@yield('content')
</body>
</html>
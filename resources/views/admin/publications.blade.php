@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row title-row">
            <h3 class="text-center">
                Publications
            </h3>
            <a href="{{ url('/admin/create-publication') }}" class="btn btn-success publications-create" role="button" aria-label="Left Align">
                <span class="glyphicon glyphicon-plus" aria-hidden="true">Create</span>
            </a>
        </div>

        <div class="row">
            <table class="table table-striped">
                <tr>
                    <th>Title</th>
                    <th>Authors</th>
                    <th>Publishing city</th>
                    <th>Publishing year</th>
                    <th>Publishing office</th>
                    <th>Number of pages</th>
                    <th>Publications type</th>
                    <th>Actions</th>
                </tr>
                @foreach($publications as $key => $value)
                    <tr>
                        <td>{!! $value->title !!}</td>
                        <td>
                            @foreach($value->authors as $author)
                                {!! $author->first_name !!}
                                {!! $author->last_name !!};
                            @endforeach
                        </td>
                        <td>{!! $value->publishing_city !!}</td>
                        <td>{!! $value->publishing_year !!}</td>
                        <td>{!! $value->publishing_office !!}</td>
                        <td>{!! $value->pages !!}</td>
                        <td>{!! $value->type !!}</td>
                        <td>
                            <a class="glyphicon glyphicon-pencil" aria-hidden="true"  href="{{ url('/admin/edit-publication/'.$value->id) }}"></a>
                            <a class="glyphicon glyphicon-remove" aria-hidden="true" href="{{ url('/admin/delete-publication/'.$value->id) }}"></a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop
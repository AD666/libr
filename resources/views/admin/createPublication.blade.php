@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row">

            @if(isset($publication))
                {!! Form::model($publication, ['method' => 'POST', 'url' =>'/admin/edit-publication/'.$publication->id, 'class' => 'form-horizontal']) !!}
            @else
                {!! Form::open(['method' => 'POST', 'url' =>'/admin/create-publication', 'class' => 'form-horizontal']) !!}
            @endif

                {!! csrf_field() !!}

                <div class="form-group">
                    <label class="col-md-1 control-label">Title</label>
                    <div class="col-md-3">
                        {!! Form::text('title', Input::old('title'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Author</label>
                    <div class="col-md-3">
                        {!! Form::select('authors[]', $authors, isset($selectedAuthors) ? $selectedAuthors  : '', ['multiple' => 'multiple', 'class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Publishing city</label>
                    <div class="col-md-3">
                        {!! Form::text('publishing_city', Input::old('publishing_city'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Publishing year</label>
                    <div class="col-md-3">
                        {!! Form::number('publishing_year', Input::old('publishing_year'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Publishing office</label>
                    <div class="col-md-3">
                        {!! Form::text('publishing_office', Input::old('publishing_office'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Number of pages</label>
                    <div class="col-md-3">
                        {!! Form::number('pages', Input::old('pages'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Graphics</label>
                    <div class="col-md-3">
                        {!! Form::text('graphics', Input::old('graphics'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Type</label>
                    <div class="col-md-3">
                        {!! Form::text('type', Input::old('type'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Contains</label>
                    <div class="col-md-3">
                        {!! Form::text('contains', Input::old('contains'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-1 col-md-offset-1">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
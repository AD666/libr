@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row title-row">
            <h3 class="text-center">
                Authors
            </h3>
            <a href="{{ url('/admin/create-author') }}" class="btn btn-success publications-create" role="button" aria-label="Left Align">
                <span class="glyphicon glyphicon-plus" aria-hidden="true">Create</span>
            </a>
        </div>

        <div class="row">
            <table class="table table-striped">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Publishings</th>
                    <th>Actions</th>
                </tr>
                @foreach($authors as $key => $value)
                    <tr>
                        <td>{!! $value->first_name !!}</td>
                        <td>{!! $value->last_name !!}</td>
                        <td>
                            @foreach($value->publications as $publication)
                                {!! $publication->title !!};
                            @endforeach
                        </td>
                        <td>
                            <a class="glyphicon glyphicon-pencil" aria-hidden="true"  href="{{ url('/admin/edit-author/'.$value->id) }}"></a>
                            <a class="glyphicon glyphicon-remove" aria-hidden="true" href="{{ url('/admin/delete-author/'.$value->id) }}"></a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop
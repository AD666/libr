@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row">

            @if(isset($author))
                {!! Form::model($author, ['method' => 'POST', 'url' =>'/admin/edit-author/'.$author->id, 'class' => 'form-horizontal']) !!}
            @else
                {!! Form::open(['method' => 'POST', 'url' =>'/admin/create-author', 'class' => 'form-horizontal']) !!}
            @endif
                {!! csrf_field() !!}

                <div class="form-group">
                    <label class="col-md-1 control-label">First Name</label>
                    <div class="col-md-3">
                        {!! Form::text('first_name', Input::old('first_name'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Last Name</label>
                    <div class="col-md-3">
                        {!! Form::text('last_name', Input::old('last_name'), ['class'=>'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-1 control-label">Publications</label>
                    <div class="col-md-3">
                        {!! Form::select('size', $publications, isset($selectedPublications) ? $selectedPublications  : '', ['multiple' => 'multiple', 'class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-1 col-md-offset-1">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
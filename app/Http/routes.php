<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SiteController@index');
Route::get('/admin/login', 'AdminController@login');

Route::post('/search', 'SiteController@postSearch');

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@publications');
    Route::get('publications', 'AdminController@publications');
    Route::get('create-publication', 'AdminController@getCreatePublication');
    Route::post('create-publication', 'AdminController@postCreatePublication');
    Route::get('delete-publication/{id}', 'AdminController@getDeletePublication');
    Route::get('edit-publication/{id}', 'AdminController@getEditPublication');
    Route::post('edit-publication/{id}', 'AdminController@postEditPublication');

    Route::get('authors', 'AdminController@authors');
    Route::get('create-author', 'AdminController@getCreateAuthor');
    Route::post('create-author', 'AdminController@postCreateAuthor');
    Route::get('delete-author/{id}', 'AdminController@getDeleteAuthor');
    Route::get('edit-author/{id}', 'AdminController@getEditAuthor');
    Route::post('edit-author/{id}', 'AdminController@postEditPublication');
});


Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

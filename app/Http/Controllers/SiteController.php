<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Publication;
use App\Author;

class SiteController extends Controller
{
    public function index()
    {
        $result = [];
        return view('index')->with('results', $result);
    }

    public function postSearch()
    {
        $query = Publication::with('authors')->select('*');
        foreach($_POST as $k => $v)
        {
            if($k != '_token' && $v != '') {
                if($k == 'pages' || $k == 'publishing_year') {
                    $query->where($k, '=', $v);
                } elseif($k == 'author') {
                    $query->whereHas('authors', function($q) use($v)
                    {
                        if($v['first_name'] != '') {
                            $q->where('first_name', 'LIKE', '%'.$v['first_name'].'%');
                        }
                        if($v['last_name'] != '') {
                            $q->where('last_name', 'LIKE', '%'.$v['last_name'].'%');
                        }
                    });
                } else {
                    $query->where($k, 'LIKE', '%'.$v.'%');
                }
            }
        }
        $result = $query->get();
        return view('index')->with('results', $result);
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Publication;
use App\Author;
use Auth;

class AdminController extends Controller
{
    public function login()
    {
        return redirect('auth/login');
    }

    public function publications()
    {
        $publications = Publication::with('authors')->get();
        return view('admin.publications')->with('publications', $publications);
    }

    public function authors()
    {
        $authors = Author::with('publications')->get();
        return view('admin.authors')->with('authors', $authors);
    }

    public function getCreatePublication()
    {
        $authors = Author::all();
        return view('admin.createPublication')->with('authors', $authors);
    }

    public function postCreatePublication()
    {
        $publication = new Publication();
        $publication->fill($_POST);
        $publication->save();
        foreach($_POST['authors'] as $author_id) {
            $author = Author::find($author_id);
            $publication->authors()->save($author);
        }
        return redirect('admin/publications');
    }

    public function getCreateAuthor()
    {
        $publications = Publication::all();
        return view('admin.createAuthor')->with('publications', $publications);;
    }

    public function postCreateAuthor()
    {
        $author = new Author();
        $author->fill($_POST);
        $author->save();
        foreach($_POST['publications'] as $publication_id) {
            $publication = Publication::find($publication_id);
            $author->publications()->save($publication);
        }
        return redirect('admin/authors');
    }

    public function getDeletePublication($publication_id)
    {
        $publication = Publication::find($publication_id);
        $publication->delete();
        return redirect('admin/publications');
    }

    public function getDeleteAuthor($author_id)
    {
        $author = Author::find($author_id);
        $author->delete();
        return redirect('admin/authors');
    }

    public function getEditPublication($publication_id)
    {
        $publication = Publication::with('authors')->find($publication_id);
        $authors = Author::selectRaw('CONCAT(first_name, " ", last_name) as full_name, id')->lists('full_name', 'id');
        foreach($publication->authors as $author)
        {
            $selectedAuthors[] = $author->id;
        }
        return view('admin.createPublication')->with('publication', $publication)->with('authors', $authors)->with('selectedAuthors', $selectedAuthors);
    }

    public function postEditPublication($publication_id)
    {
        $publication = Publication::with('authors')->find($publication_id);
        $publication->fill($_POST);
        $publication->save();
        foreach($publication->authors as $oldAuthor)
        {
            $oldAuthor->delete();
        }
        foreach($_POST['authors'] as $author_id) {
            $author = Author::find($author_id);
            $publication->authors()->save($author);
        }
        return redirect('admin/publications');
    }

    public function getEditAuthor($author_id)
    {
        $author = Author::with('publications')->find($author_id);
        $publications = Publication::lists('title', 'id');
        foreach($author->publications as $publication)
        {
            $selectedPublications[] = $publication->id;
        }
        return view('admin.createAuthor')->with('author', $author)->with('publications', $publications)->with('selectedPublications', $selectedPublications);
    }

    public function postEditAuthor($author_id)
    {
        $author = Author::with('publications')->find($author_id);
        $author->fill($_POST);
        $author->save();
        foreach($author->publications as $oldPublication)
        {
            $oldPublication->delete();
        }
        foreach($_POST['publications'] as $publication_id) {
            $publication = Publication::find($publication_id);
            $author->publications()->save($publication);
        }
        return redirect('admin/authors');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public $timestamps = false;
    protected $fillable = array('first_name', 'last_name');

    public function publications() {
        return $this->belongsToMany('\App\Publication', 'publication_author', 'publication_id', 'author_id');
    }
}

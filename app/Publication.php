<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Author;

class Publication extends Model
{
    public $timestamps = false;
    protected $fillable = array('title', 'publishing_city', 'publishing_year', 'publishing_office', 'pages', 'graphics', 'type', 'contains');

    public function authors()
    {
        return $this->belongsToMany('App\Author', 'publication_author', 'publication_id', 'author_id');
    }
}
